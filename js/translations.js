var Translator = function() {
    this.language = 'nl';
};
   var translations = {
       'app.title':{
           'nl': 'Reken Sergeant',
           'fr': 'Sergent de Math'
       },
       'app.settings':{
           'nl': 'Instellingen',
           'fr': 'Configuration'
       },
       'settings.generatedNumbers':{
           'nl': 'Gegenereerde getallen liggen tussen',
           'fr': 'Les chifres géńérés se trouvent entre'
       },
       'global.and':{
           'nl': 'en',
           'fr': 'et'
       },
       'settings.results':{
           'nl': 'De uitkomst ligt tussen',
           'fr': 'Le résultat se trouve entre'
       },
       'settings.sessionCounts':{
           'nl': 'Eén sessie telt',
           'fr': 'Une session contient'
       },
       'settings.exercises':{
           'nl': 'opgaven',
           'fr': 'exercises'
       },
       'settings.sessionTime':{
           'nl': 'Eén sessie duurt',
           'fr': 'Une session prend'
       },
       'settings.minutes':{
           'nl': 'minuten',
           'fr': 'minutes'
       },
       'global.in':{
           'nl': 'in',
           'fr': 'en'
       },
       'global.all':{
           'nl': 'Alles',
           'fr': 'Tout'
       },
       'settings.sums':{
           'nl': 'Sommen',
           'fr': 'Additions'
       },
       'settings.subtractions':{
           'nl': 'Aftrekken',
           'fr': 'Soustraction'
       },
       'settings.multiplications':{
           'nl': 'Vermenigvuldigen',
           'fr': 'Multiplication'
       },
       'settings.divisions':{
           'nl': 'Breuken (noemer tussen 1 en 10)',
           'fr': 'Fractions (dénominateur entre 1 et 10'
       },
       'settings.multiplicationTable':{
           'nl': 'Maaltafel van',
           'fr': 'Table de multiplication de'
       },
       'settings.divisionTable':{
           'nl': 'Deeltafel van',
           'fr': 'Table de fractions de'
       },
       'app.start':{
           'nl': 'start',
           'fr': 'commencer'
       },
       'app.finished':{
           'nl': 'Klaar!',
           'fr': 'Fini!'
       }
   };
   Translator.prototype.getTranslation = function(label) {

        if ( label && translations[label] && translations[label][this.language] ) {
            return translations[label][this.language];

        }
    };
   Translator.prototype.setLanguage = function(lang) {
       var translator = this;
       translator.language = lang;
       $('[translate]').each(function(){
           var label = $(this).attr('translate');
           var translation = translator.getTranslation(label);
           if ( translation ) {
               $(this).text(translation);
           }
       })
   };
